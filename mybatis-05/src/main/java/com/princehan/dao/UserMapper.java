package com.princehan.dao;

import com.princehan.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/3/31 20:42
 */
public interface UserMapper {
    @Select("select id, name, pwd password from user")
    List<User> getUsers();

    //多个参数要用 @Param()
    @Select("select * from user where id = #{id}")
    User getUserByID(@Param("id") int id);

    @Insert("insert into user(id, name, pwd) values(#{id}, #{name}, #{password})")
    int addUser(User user);

    @Update("update user set name = #{name}, pwd = #{password} where id = #{id}")
    int updateUser(User user);

    @Delete("delete from user where id = #{uid}")//如果加@Param()要与里面的key相同
    int deleteUser(@Param("uid") int id);
}
