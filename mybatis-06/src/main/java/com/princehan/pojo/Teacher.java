package com.princehan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.ConstructorArgs;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/10 10:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
        private int id;
        private String name;

}
