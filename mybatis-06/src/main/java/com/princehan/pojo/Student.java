package com.princehan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/10 10:21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Student {
        private int id;
        private String name;
        private Teacher teacher;
}
