package com.princehan.dao;

import com.princehan.pojo.Student;
import com.princehan.pojo.Teacher;

import java.util.List;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/10 10:24
 */
public interface StudentMapper {
    List<Student> getStudents1();
    List<Student> getStudents2();
}
