package com.princehan.dao;

import com.princehan.pojo.User;

import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/3/29 19:11
 */
public interface UserMapper {
    List<User> getUserList();

    List<User> getUserLike(String like);

    User getUserById(int id);

    int addUser(User user);

    int addUser2(Map<String, Object> map);

    int updateUser(User user);

    int deleteUser(int id);
}
