package com.princehan.dao;

import com.princehan.pojo.User;

import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/3/31 20:42
 */
public interface UserMapper {

    User getUserById(int id);

    List<User> getUserByLimit(Map<String, Integer> map);
}
