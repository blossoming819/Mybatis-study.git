package com.princehan.dao;

import com.princehan.pojo.User;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/3/31 20:42
 */
public interface UserMapper {
    User getUserById(int id);
}
