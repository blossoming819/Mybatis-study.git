package com.princehan.dao;

import com.princehan.pojo.Teacher;
import org.apache.ibatis.annotations.Param;


/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/10 18:38
 */
public interface TeacherMapper {

    //查询指定老师下的学生及老师信息
    Teacher getTeacher(@Param("tid") int id);
    Teacher getTeacher2(@Param("tid") int id);
}
