package com.princehan.pojo;

import lombok.Data;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/10 18:28
 */
@Data
public class Student {
    private int id;
    private String name;
    private int tid;

}
