package com.princehan.pojo;

import lombok.Data;

import java.util.List;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/10 18:31
 */
@Data
public class Teacher {
    private int id;
    private String name;
    private List<Student> students;
}
