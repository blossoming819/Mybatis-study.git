package com.princehan.dao;

import com.princehan.pojo.User;
import org.apache.ibatis.annotations.Param;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/11 21:18
 */
public interface UserMapper {
    //根据id查询用户
    User queryUserById(@Param("id") int id);
}
