import com.princehan.dao.UserMapper;
import com.princehan.pojo.User;
import com.princehan.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/11 21:29
 */
public class MyTest {
    @Test
    public void queryUserById() {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        SqlSession sqlSession2 = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        UserMapper mapper2 = sqlSession2.getMapper(UserMapper.class);
        User user = mapper.queryUserById(1);
        System.out.println(user);
        sqlSession.close();
        System.out.println("========");
        User user1 = mapper2.queryUserById(1);
        System.out.println(user1);
        System.out.println(user == user1);
        sqlSession2.close();
    }
}
