package com.princehan.dao;

import com.princehan.pojo.Blog;

import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/10 22:45
 */
@SuppressWarnings("all")
public interface BlogMapper {
    //插入数据
    int addBlog(Blog blog);

    //更新博客
    int updateBlog(Map map);

    //查询博客
    List<Blog> queryBlogIF(Map map);

    //查询博客
    List<Blog> queryBlogChoose(Map map);

    //查询1-2-3号记录的博客
    List<Blog> queryBlogForeach(Map map);
}
