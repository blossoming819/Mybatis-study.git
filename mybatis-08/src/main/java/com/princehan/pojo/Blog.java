package com.princehan.pojo;

import lombok.Data;

import java.util.Date;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/10 22:43
 */
@Data
public class Blog {
    private String id;
    private String title;
    private String author;
    private Date createTime;//属性名与字段名不一致
    private int views;
}
