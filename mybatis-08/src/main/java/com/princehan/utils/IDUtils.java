package com.princehan.utils;

import org.junit.Test;

import java.util.UUID;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/4/10 22:49
 */
@SuppressWarnings("all") //抑制警告
public class IDUtils {
    public static String getID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    @Test
    public void test() {
        System.out.println(IDUtils.getID());
        System.out.println(IDUtils.getID());
        System.out.println(IDUtils.getID());
    }
}
