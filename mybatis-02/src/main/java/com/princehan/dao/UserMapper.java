package com.princehan.dao;

import com.princehan.pojo.User;

import java.util.List;

/**
 * @Description
 * @Author:PrinceHan
 * @CreateTime:2022/3/29 19:11
 */
public interface UserMapper {
    List<User> getUserList();

    User getUserById(int id);

    int addUser(User user);

    int updateUser(User user);

    int deleteUser(int id);
}
